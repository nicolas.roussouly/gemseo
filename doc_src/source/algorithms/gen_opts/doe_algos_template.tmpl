..
   Copyright 2021 IRT Saint-Exupéry, https://www.irt-saintexupery.com

   This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
   Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. _gen_doe_algos:

DOE algorithms options
======================

A simple way to solve an :class:`.OptimizationProblem` through a DOE
is to use the API method :meth:`~gemseo.api.execute_algo`
with any choice of DOE algorithm.
E.g.

.. code::

	from gemseo.api import execute_algo
	from gemseo.problems.analytical.rosenbrock import Rosenbrock

	problem = Rosenbrock()
	sol = execute_algo(problem, "OT_LHS", n_samples=20)

In addition to these arguments, many options can be prescribed depending on the choice
of the algorithm. The list of all the optional arguments are given below.

An other way to execute a DOE algorithm is through a :class:`.DOEScenario`,
calling the :code:`execute()` function.
In such case, any optional parameters can be prescribed through the argument :code:`algo_options`
with a dictionary.
E.g.

.. code::

    from gemseo.api import create_discipline, read_design_space, \
    create_scenario, configure_logger

    discipline = create_discipline("SobieskiStructure")

    design_space = read_design_space("sobieski_design_space.txt")

    scenario = create_scenario(discipline, "DisciplinaryOpt", "y_1", design_space, scenario_type="DOE")
    scenario.execute({"algo": "fullfact", "n_samples": 2, "algo_options": {"ftol_rel":1e-6, "ineq_tolerance":1e-3}})


List of available algorithms:
{% for algo in doe_algos%}:ref:`{{algo}}_options` -
{% endfor %}

{% for algo in doe_algos%}
.. _{{algo}}_options:

{{algo}}
{{ (algo|length)*'-' }}

Description
~~~~~~~~~~~

{{doe_descriptions[algo]}}

{% if doe_url[algo] is not none %}
{% set temp = '`External link <' + doe_url[algo] + '>`__' %}
{{temp}}
{{ (temp|length)*'~' }}
{%endif %}

Options
~~~~~~~

{% for option in doe_options[algo]|dictsort %}
- **{{option[0]}}**, :code:`{{option[1]['ptype']}}` - {{option[1]['description']}}

{% endfor %}
{% endfor %}
